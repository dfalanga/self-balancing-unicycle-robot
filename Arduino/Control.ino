

// Control algorithm

#include <Wire.h>
#include "Kalman.h" // Source: https://github.com/TKJElectronics/KalmanFilter

#define periodo 0
#define tempo_calcolo 1.69

int c;
// Encoder PINs setting
//Encoder 8192 ppr
int encoder1PinA = 52;
int encoder1PinB = 53;
//Encoder 1216 ppr
//int encoder1PinA = 44;
//int encoder1PinB = 45;
int encoder2PinA = 48;
int encoder2PinB = 49;

// Motor PIN setting
int MYINA = 2;
int MYINB = 4;
int MYENA = 6;
int MYPWM = 9;
int MXINA = 7;
int MXINB = 8;
int MXENA = 12;
int MXPWM = 10;

int dutyX=0;
int dutyY=0;

Kalman kalmanX;
Kalman kalmanY;
float pi=3.1415;

const uint8_t IMUAddress = 0x68;

/* IMU Data */
int16_t accX;
int16_t accY;
int16_t accZ;
int16_t gyroX;
int16_t gyroY;
int16_t gyroZ;

double accXangle; // Angle calculate using the accelerometer
double accYangle;
double gyroXangle = 180; // Angle calculated using the gyro
double gyroYangle = 180;
double compAngleX = 180; // Calculate the angle using a Kalman filter
double compAngleY = 180;

uint32_t timer;

double kalAngleX=0; // Calculate the angle using a Kalman filter
double kalAngleY=0;
double gyroXrate=0;
double gyroYrate=0;
long int posX=0;
long int posY=0;
double speedX=0;
double speedY=0;

//double speedX_hold=0;
//double speedY_hold=0;
double gyroXrate_hold=0;
double gyroYrate_hold=0;

double coppia_ruota;
double coppia_disco;
double tensione_ruota;
double tensione_disco;

double IntAlpha2=0;
//double KI=0.15;

// Motor parameters, experimentally identificated
double Ra=2.4;
double Km=0.0066;
double Ke=0.0099;
double k=19;

// State
double stato_ruota[4][1];
double stato_disco[4][1];
double guadagni_ruota[1][4];
double guadagni_disco[1][4];



////////////////////////////////////////////////////////////////
//                                                            //
//                                                            //
//                     SETUP FUNCTION                         //
//                                                            //
////////////////////////////////////////////////////////////////

void setup() {

  //Setup IMU
  Serial.begin(115200);
  Wire.begin();  
  i2cWrite(0x6B,0x00); // Disable sleep mode  
  if(i2cRead(0x75,1)[0] != 0x68) { // Read "WHO_AM_I" register
    Serial.print("MPU-6050 with address 0x");
    Serial.print(IMUAddress,HEX);
    Serial.println(" is not connected");
    while(1);
  }      
  kalmanX.setAngle(180); // Set starting angle
  kalmanY.setAngle(180);
  timer = micros();
  
  // Gestione PIN per l'encoder
  pinMode (encoder1PinA,INPUT);
  pinMode (encoder1PinB,INPUT);
  pinMode (encoder2PinA,INPUT);
  pinMode (encoder2PinB,INPUT);
  
  pinMode(MXINA, OUTPUT); 
  pinMode(MXINB, OUTPUT);     
  pinMode(MXENA, OUTPUT);     
  pinMode(MXPWM, OUTPUT);   
  pinMode(MYINA, OUTPUT); 
  pinMode(MYINB, OUTPUT);     
  pinMode(MYENA, OUTPUT);     
  pinMode(MYPWM, OUTPUT);  
  
  // Interruptions on 2-3 PINs 
  attachInterrupt(encoder1PinA, GestInt1, CHANGE);
  attachInterrupt(encoder1PinB, GestInt1, CHANGE);
  //attachInterrupt(encoder2PinA, GestInt2, CHANGE);
  //attachInterrupt(encoder2PinB, GestInt2, CHANGE);
  
  // Gains for LQR control
    guadagni_ruota[0][0]=-0.01;
    guadagni_ruota[0][1]=-4.3057;
    guadagni_ruota[0][2]=-0.0204;
    guadagni_ruota[0][3]=-0.03401;
   
    guadagni_disco[0][0]=0;
    guadagni_disco[0][1]=0;
    guadagni_disco[0][2]=0;
    guadagni_disco[0][3]=0;
  
  Serial.println("Avvio in corso...");
}

////////////////////////////////////////////////////////////////
//                                                            //
//                   MAIN LOOP                                //
//                                                            //
////////////////////////////////////////////////////////////////

void loop() {
  // IMU reading and filtering
  /* Update all the values */
  uint8_t* data = i2cRead(0x3B,12);  
  accX = ((data[0] << 8) | data[1]);
  accY = ((data[2] << 8) | data[3]);
  accZ = ((data[4] << 8) | data[5]);  
  gyroX = ((data[8] << 8) | data[9]);
  gyroY = ((data[10] << 8) | data[11]);
  //gyroZ = ((data[12] << 8) | data[13]);
  
  /* Calculate the angls based on the different sensors and algorithm */
    accYangle = (atan2(accX,accZ)+PI)*RAD_TO_DEG;
    // accXangle = (atan2(accY,accZ)+PI)*RAD_TO_DEG;    
    //Calculate gyroRate and gyroAngle
    //gyroXrate = (double)gyroX/131.0;
    gyroYrate = -((double)gyroY/131.0);
    //gyroXangle += gyroXrate*((double)(micros()-timer)/1000000); // Calculate gyro angle without any filter  
    //gyroYangle += gyroYrate*((double)(micros()-timer)/1000000);
    //gyroXangle += kalmanX.getRate()*((double)(micros()-timer)/1000000); // Calculate gyro angle using the unbiased rate
    gyroYangle += kalmanY.getRate()*((double)(micros()-timer)/1000000);
    //kalAngleX = kalmanX.getAngle(accXangle, gyroXrate, (double)(micros()-timer)/1000000); // Calculate the angle using a Kalman filter
    kalAngleY = kalmanY.getAngle(accYangle, gyroYrate, (double)(micros()-timer)/1000000);
    timer = micros();
  
    // Encoder
    speedY=(double)Calc_speedY();
    //speedX=(double)Calc_speedX();
    posY=Calc_posY();
    //posX=Calc_posX();
    
    //gyroXrate=(gyroXrate+gyroXrate_hold)/2;
    gyroYrate=(gyroYrate+gyroYrate_hold)/2;
    // gyroXrate_hold=gyroXrate;
    gyroYrate_hold=gyroYrate;

    // Gear state vector
    stato_ruota[0][0]=(((double)posY/8192))*2*pi;
    stato_ruota[1][0]=((kalAngleY-180)/360)*2*pi-0.0424+0.0025;
    stato_ruota[2][0]=(speedY)*2*pi/60;
    stato_ruota[3][0]=(gyroYrate/360)*2*pi;
    //if(stato_ruota[1][0]>=-0.01&&stato_ruota[1][0]<=0.01){stato_ruota[1][0]=0;}
    
    // Disc state vector
    /* stato_disco[0][0]=((kalAngleX-180)/360)*2*pi+0.02;
    stato_disco[1][0]=-(((double)posX/1216))*2*pi;
    stato_disco[2][0]=(gyroXrate/360)*2*pi+0.02;
    stato_disco[3][0]=-(speedX)*2*pi/60;
    */
   
    // Control actions computation
    int i=0;
    double temp_ruota=0;
    double temp_disco=0;
    for (i=0;i<4;i++){
    temp_ruota=stato_ruota[i][0]*guadagni_ruota[0][i]+temp_ruota;
    //temp_disco=stato_disco[i][0]*guadagni_disco[0][i]+temp_disco;
    }
    
    if(coppia_ruota>0.6||coppia_ruota<-0.6){
      IntAlpha2=IntAlpha2;
    }else{
    IntAlpha2=IntAlpha2+stato_ruota[1][0]*KI*(periodo+tempo_calcolo);
    }
    coppia_ruota=temp_ruota-IntAlpha2;
   
    // Control deactivation over a suitable angle
    if(stato_ruota[1][0]>0.3||stato_ruota[1][0]<-0.3){digitalWrite(MYENA,LOW);} else{digitalWrite(MYENA,HIGH);}
    // if(stato_disco[0][0]>0.3||stato_disco[0][0]<-0.3){digitalWrite(MXENA,LOW);} else{ digitalWrite(MXENA,HIGH);}
   
    /*
    if (coppia_ruota>0){
                       coppia_ruota=coppia_ruota+0.06;
    }else if (coppia_ruota<0){
                     coppia_ruota=coppia_ruota-0.06;
    }else{
                 coppia_ruota=coppia_ruota;
                     }
                     
     */ 
     
    // Torque to voltage conversion
    tensione_ruota=(Ra*coppia_ruota/(Km*k))+((Ke*k)*stato_ruota[2][0]);
    /*tensione_disco=-(Ra*coppia_disco/Km)+(Ke*stato_disco[3][0]);
    tensione_ruota=(Ra*coppia_ruota/(Km*k));
    tensione_disco=-(Ra*coppia_disco/(Km*k));
   
      if(tensione_disco>=0){
      digitalWrite(MXINA,HIGH);
      digitalWrite(MXINB,LOW);
     }else{
      digitalWrite(MXINA,LOW);
      digitalWrite(MXINB,HIGH);
    }
    */
    
    if(tensione_ruota>=0){
     digitalWrite(MYINA,HIGH);
     digitalWrite(MYINB,LOW);
    }else{
      digitalWrite(MYINA,LOW);
      digitalWrite(MYINB,HIGH);
    }
    
    // PWM duty cycle
    //dutyX=(double)abs(tensione_disco)/12*255;
    dutyY=(double)abs(tensione_ruota)/12*255;
        
    //Saturation
    //if(dutyX>255){dutyX=255;}
    if(dutyY>255){dutyY=255;}
    
    // Control actions
    //analogWrite(MXPWM, dutyX);  
    analogWrite(MYPWM, dutyY); 
    
  
// Print
if(c==50){ 
  c=0; 

  Serial.print("Y angle (wheel): ");
  Serial.print(stato_ruota[1][0],4);Serial.print("\t");
   
  //Serial.print("Disc position: ");
  //Serial.print(stato_disco[1][0]);Serial.print("\t");
  Serial.print("Wheel position: ");
  Serial.print(stato_ruota[0][0]);Serial.print("\t");
    
  //Serial.print("Disc gyro: ");
  //Serial.print(stato_disco[2][0]);Serial.print("\t");
  Serial.print("Wheel gyro: ");
  Serial.print(stato_ruota[3][0]);Serial.print("\t");
  
  //Serial.print("Disc speed: ");
  //Serial.print(stato_disco[3][0]);Serial.print("\t");
  Serial.print("Wheel speed: ");
  Serial.print(stato_ruota[2][0]);Serial.print("\t");
  
  
  //Serial.print("Disc control action: ");
  //Serial.print(tensione_disco);Serial.print("\t");
  Serial.print("Wheel control action: ");
  Serial.print(tensione_ruota);Serial.print("\n");
  
 
}else{c++;}
  
  delay(periodo);
}


// IMU functions
void i2cWrite(uint8_t registerAddress, uint8_t data){
  Wire.beginTransmission(IMUAddress);
  Wire.write(registerAddress);
  Wire.write(data);
  Wire.endTransmission();
}

uint8_t* i2cRead(uint8_t registerAddress, uint8_t nbytes) {
  uint8_t data[nbytes];  
  Wire.beginTransmission(IMUAddress);
  Wire.write(registerAddress);
  Wire.endTransmission(false);
  Wire.requestFrom(IMUAddress, nbytes);
  for(uint8_t i = 0; i < nbytes; i++)
    data[i] = Wire.read();
  return data;
} 





