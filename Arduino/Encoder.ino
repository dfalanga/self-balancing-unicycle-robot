 // This is the encoder module
 
 int encoder1PinALast1 = LOW;
 int encoder1PinBLast1 = LOW;
 int encoder1PinALast2 = LOW;
 int encoder1PinBLast2 = LOW;
 int n1 = LOW;
 int n2 = LOW;
 int n3 = LOW;
 int n4 = LOW;

 int encoder2PinALast1 = LOW;
 int encoder2PinBLast1 = LOW;
 int encoder2PinALast2 = LOW;
 int encoder2PinBLast2 = LOW;
 int n5 = LOW;
 int n6 = LOW;
 int n7 = LOW;
 int n8 = LOW;

 long int encoder1Pos = 0;
 long int encoder2Pos = 0;
 double n_impulsi1=0;
 double speed1=0;
 int temp1=0;

 double n_impulsi2=0;
 double speed2=0;
 int temp2=0;

////////////////////////////////////////////////////////////////
//                                                            //
//                                                            //
//                 Data reading from encoders                 //
//                                                            //
////////////////////////////////////////////////////////////////

// Encoder 1
void GestInt1(){
  n1 = digitalRead(encoder1PinA);
  if ((encoder1PinALast1 == LOW) && (n1 == HIGH)) {
     if (digitalRead(encoder1PinB) == LOW) {
       encoder1Pos--;       
      } else {
       encoder1Pos++;           
     }
   } 
   encoder1PinALast1 = n1;
   
  n2 = digitalRead(encoder1PinB);
  if ((encoder1PinBLast1 == LOW) && (n2 == HIGH)) {
     if (digitalRead(encoder1PinA) == HIGH) {
       encoder1Pos--;         
      } else {
       encoder1Pos++;          
     }
   } 
   encoder1PinBLast1 = n2;
   
  n3 = digitalRead(encoder1PinA);
  if ((encoder1PinALast2 == HIGH) && (n3 == LOW)) {
     if (digitalRead(encoder1PinB) == HIGH) {
       encoder1Pos--;       
      } else {
       encoder1Pos++;           
     }
   } 
   encoder1PinALast2 = n3;
   
  n4 = digitalRead(encoder1PinB);
  if ((encoder1PinBLast2 == HIGH) && (n4 == LOW)) {
     if (digitalRead(encoder1PinA) == LOW) {
       encoder1Pos--;       
      } else {
       encoder1Pos++;           
     }
   } 
   encoder1PinBLast2 = n4;
   //Serial.println(encoder1Pos);;
}




// Encoder 2
void GestInt2(){
  
  n5 = digitalRead(encoder2PinA);
  if ((encoder2PinALast1 == LOW) && (n5 == HIGH)) {
     if (digitalRead(encoder2PinB) == LOW) {
       encoder2Pos--;   
      } else {
       encoder2Pos++;        
     }
   } 
   encoder2PinALast1 = n5;
   
  n6 = digitalRead(encoder2PinB);
  if ((encoder2PinBLast1 == LOW) && (n6 == HIGH)) {
     if (digitalRead(encoder2PinA) == HIGH) {
       encoder2Pos--;
      } else {
       encoder2Pos++;            
     }
   } 
   encoder2PinBLast1 = n6;
   
  n7 = digitalRead(encoder2PinA);
  if ((encoder2PinALast2 == HIGH) && (n7 == LOW)) {
     if (digitalRead(encoder2PinB) == HIGH) {
       encoder2Pos--;         
      } else {
       encoder2Pos++;          
     }
   } 
   encoder2PinALast2 = n7;
   
  n8 = digitalRead(encoder2PinB);
  if ((encoder2PinBLast2 == HIGH) && (n8 == LOW)) {
     if (digitalRead(encoder2PinA) == LOW) {
       encoder2Pos--;      
      } else {
       encoder2Pos++;           
     }
   } 
   encoder2PinBLast2 = n8;
   //Serial.println(encoder2Pos);
}

////////////////////////////////////////////////////////////////
//                                                            //
//                                                            //
//       Position and velocity (dirty derivate)               //
//                                                            //
////////////////////////////////////////////////////////////////

// Encoder 1
double Calc_posY(){
 return encoder1Pos;
}


double Calc_speedY(){
  n_impulsi1=encoder1Pos-temp1;
  speed1=((n_impulsi1/(periodo+tempo_calcolo))*1000*60)/8192;
  temp1=encoder1Pos;
  return speed1;
  
  
}

// Encoder 2
double Calc_posX(){
 return encoder2Pos;
}
double Calc_speedX(){
  n_impulsi2=encoder2Pos-temp2;
  speed2=((n_impulsi2/(periodo+tempo_calcolo))*1000*60)/1216;
  temp2=encoder2Pos;
  return speed2;
  
   
}

