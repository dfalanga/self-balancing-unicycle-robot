% Model linearization - longitudinal motion
%
% Falanga Davide - M58/48
% Fontanelli Andrea - M58/64

clear all;
close all;
clc;

%Tempo di campionamento
Tc=2*10^-3;

% Imposto i parametri del sistema 
Mbt = 1.475; % Massa del corpo
M1 = 1.475; % Massa del corpo
M2 = 0.342; % Massa del disco
Mw = 0.281; % Massa della ruota
Rw = 0.05; % Raggio della ruota
Jw = 0.000262; % Momento di inerzia della ruota intorno al baricentro
Jbt = 0.017097; % Momento di inerzia del corpo intorno al baricentro
J1a = 0.051625; % Momento di inerzia del corpo intorno al punto A
J2c = 0.003303; % Momento di inerzia del disco intorno al suo centro
L1 = 0.2916; % Distanza tra il punto A ed il centro del disco
L1c = 0.153; % Distanza tra il punto A ed il baricentro del corpo
Lbt = 0.103; % Distanza tra il centro della ruota ed il baricentro del corpo
g = 9.81; % Accelerazione di gravita'

% Dati del motore
kr = 19; % Rapporto di riduzione
Vp = 12; % Tensione di alimentazione [V]
I_max = 1.33; % Corrente massima [A]
La = 0.556 *1e-3 ; % Induttanza [H]
Ra = 2.4; % Resistenza [Ohm]
Ta = La/Ra; % Costante di tempo elettrica [s]
Kv =  1/(992.0*2*pi/60); % Costante di velocita' [Vs/rad]
Kt = 6.3 *1e-3; % Costante di coppia [Nm/A]
Tm = 14.6 * 1e-3; % Costante di tempo meccanica [s]
Jm = 45.3 * 1e-3*1e-4;  % Inerzia del motore [kg m^2]
C_max = I_max * Kt * kr; % Massima coppia continuativa [Nm]
Fm = 0; % Coefficiente di frizione del rotore [kg/sm^2]
C_max = I_max * Kt * kr; %Maximum continuos torque [Nm]

% Effettuo la linearizzazione
x0=[0;0;0;0];
u0=[0];
[Ar,Br,Cr,Dr]=linmod('ruota_dee',x0,u0);
ruota=ss(Ar,Br,Cr,Dr)

% Osservabilita'
obsv_mat=obsv(ruota);
no_obsv=length(Ar)-rank(obsv_mat);

if no_obsv == 0
    disp 'Sistema osservabile.';
else
    disp 'Sistema osservabile.';
end

% Controllabilita'
contr_mat=ctrb(ruota);
no_contr=length(Ar)-rank(contr_mat);

if no_contr == 0
    disp 'Sistema controllabile.';
else
    disp 'Sistema non controllabile.';
end

% Controllo LQR 
Qr=diag([2,20000,2,1]);
Rr=diag([2000]);
Kr=lqr(ruota,Qr,Rr);

%Assegnamento dei poli
Pr=[-0.5,-50,-0.6,-10];

Kr_p=place(Ar,Br,Pr);

%Controllo assegnamento dei poli
eig(Ar-Br*Kr_p)
