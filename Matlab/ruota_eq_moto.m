% --------------------------------------------------------
% This file lets you evaluate the expression of the state equations for the
% wheel. These are sent to DEE to extract the linearized model.

%  Longitudinal motion
% (Mbt*Rw^2 + Mw*Rw^2 + Jw)*Alfa1DuePunti + (Mbt*Rw*Lbt*cos(Alfa2) + Mbt*Rw^2 + Mw*Rw^2 + Jw)*Alfa2DuePunti - Mbt*Rw*Lbt*Alfa2Punto^2*sin(Alfa2) = TauW
% (Mbt*Rw*Lbt*cos(Alfa2) + Mbt*Rw^2 + Mw*Rw^2 + Jw)*Alfa1DuePunti + (2*Mbt*Rw*Lbt*cos(Alfa2) + Mbt*Rw^2 + Mbt*Lbt^2 + Jbt + Mw*Rw^2 + Jw)*Alfa2DuePunti - Mbt*Rw*Lbt*Alfa2Punto^2*sin(Alfa2) - Mbt*g*Lbt*sin(Alfa2) = 0
%  Lateral motion
% (J1a + J2c + M2*L1^2)*Beta1DuePunti + J2c*Beta2DuePunti - (M1*L1c + M2*L1)*g*sin(Beta1) = 0
% J2c*Beta1DuePunti + J2c*Beta2DuePunti = TauD
% --------------------------------------------------------

clc;
close all;
clear all;

syms Alfa1 Alfa2 Alfa1Punto Alfa2Punto  Alfa1DuePunti Alfa2DuePunti;
syms Mbt M1 M2 Mw Rw Jw Jbt J1a J2c L1 L1c Lbt TauW TauD g;


% Moto Longitudinale (corpo+ruota)
% Alfa1 = angolo tra la ruota ed il corpo
% Alfa2 = angolo tra il corpo ed il riferimento verticale

eqr1 = sym('(Mbt*Rw^2 + Mw*Rw^2 + Jw)*Alfa1DuePunti + (Mbt*Rw*Lbt*cos(Alfa2) + Mbt*Rw^2 + Mw*Rw^2 + Jw)*Alfa2DuePunti - Mbt*Rw*Lbt*Alfa2Punto^2*sin(Alfa2) = TauW');
eqr2 = sym('(Mbt*Rw*Lbt*cos(Alfa2) + Mbt*Rw^2 + Mw*Rw^2 + Jw)*Alfa1DuePunti + (2*Mbt*Rw*Lbt*cos(Alfa2) + Mbt*Rw^2 + Mbt*Lbt^2 + Jbt + Mw*Rw^2 + Jw)*Alfa2DuePunti - Mbt*Rw*Lbt*Alfa2Punto^2*sin(Alfa2) - Mbt*g*Lbt*sin(Alfa2) = 0');

eqr1 = subs(eqr1, 'Alfa2DuePunti', solve(eqr2,'Alfa2DuePunti'));
eqr2 = subs(eqr2, 'Alfa1DuePunti', solve(eqr1,'Alfa1DuePunti'));

Alfa1DuePunti = solve(eqr1, 'Alfa1DuePunti');
Alfa2DuePunti = solve(eqr2, 'Alfa2DuePunti');

% Alfa1 = x1
% Alfa2 = x2
% Alfa1Punto = x3
% Alfa2Punto = x4
% Beta1 = x5
% Beta2 = x6
% Beta1Punto = x7
% Beta2Punto = x8
% TauD = u1
% TauW = u2

Alfa1DuePunti = subs(Alfa1DuePunti,Alfa1,sym('x(1)'));
Alfa1DuePunti = subs(Alfa1DuePunti,Alfa2,sym('x(2)'));
Alfa1DuePunti = subs(Alfa1DuePunti,Alfa1Punto,sym('x(3)'));
Alfa1DuePunti = subs(Alfa1DuePunti,Alfa2Punto,sym('x(4)'));
Alfa1DuePunti = subs(Alfa1DuePunti,TauW,sym('u(1)'));

Alfa2DuePunti = subs(Alfa2DuePunti,Alfa1,sym('x(1)'));
Alfa2DuePunti = subs(Alfa2DuePunti,Alfa2,sym('x(2)'));
Alfa2DuePunti = subs(Alfa2DuePunti,Alfa1Punto,sym('x(3)'));
Alfa2DuePunti = subs(Alfa2DuePunti,Alfa2Punto,sym('x(4)'));
Alfa2DuePunti = subs(Alfa2DuePunti,TauW,sym('u(1)'));

