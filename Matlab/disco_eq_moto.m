% --------------------------------------------------------
% This file lets you evaluate the expression of the state equations for the
% disc. These are sent to DEE to extract the linearized model.
%
%  Longitudinal motion
% (Mbt*Rw^2 + Mw*Rw^2 + Jw)*Alfa1DuePunti + (Mbt*Rw*Lbt*cos(Alfa2) + Mbt*Rw^2 + Mw*Rw^2 + Jw)*Alfa2DuePunti - Mbt*Rw*Lbt*Alfa2Punto^2*sin(Alfa2) = TauW
% (Mbt*Rw*Lbt*cos(Alfa2) + Mbt*Rw^2 + Mw*Rw^2 + Jw)*Alfa1DuePunti + (2*Mbt*Rw*Lbt*cos(Alfa2) + Mbt*Rw^2 + Mbt*Lbt^2 + Jbt + Mw*Rw^2 + Jw)*Alfa2DuePunti - Mbt*Rw*Lbt*Alfa2Punto^2*sin(Alfa2) - Mbt*g*Lbt*sin(Alfa2) = 0
%  Lateral motion
% (J1a + J2c + M2*L1^2)*Beta1DuePunti + J2c*Beta2DuePunti - (M1*L1c + M2*L1)*g*sin(Beta1) = 0
% J2c*Beta1DuePunti + J2c*Beta2DuePunti = TauD
% --------------------------------------------------------

clc;
close all;
clear all;

syms Beta1 Beta2  Beta1Punto Beta2Punto Beta1DuePunti Beta2DuePunti;
syms Mbt M1 M2 Mw Rw Jw Jbt J1a J2c L1 L1c Lbt TauW TauD g;

eqd1 = sym('(J1a + J2c + M2*(L1^2))*Beta1DuePunti + J2c*Beta2DuePunti - (M1*L1c + M2*L1)*g*sin(Beta1) = 0');
eqd2 = sym('J2c*Beta1DuePunti + J2c*Beta2DuePunti = TauD');

eqd1 = subs(eqd1, 'Beta2DuePunti', solve(eqd2,'Beta2DuePunti'));
eqd2 = subs(eqd2, 'Beta1DuePunti', solve(eqd1,'Beta1DuePunti'));

Beta1DuePunti = solve(eqd1, 'Beta1DuePunti');
Beta2DuePunti = solve(eqd2, 'Beta2DuePunti');

Beta1DuePunti = subs(Beta1DuePunti,Beta1,sym('x(1)'));
Beta1DuePunti = subs(Beta1DuePunti,Beta2,sym('x(2)'));
Beta1DuePunti = subs(Beta1DuePunti,Beta1Punto,sym('x(3)'));
Beta1DuePunti = subs(Beta1DuePunti,Beta2Punto,sym('x(4)'));
Beta1DuePunti = subs(Beta1DuePunti,TauD,sym('u(1)'));

Beta2DuePunti = subs(Beta2DuePunti,Beta1,sym('x(1)'));
Beta2DuePunti = subs(Beta2DuePunti,Beta2,sym('x(2)'));
Beta2DuePunti = subs(Beta2DuePunti,Beta1Punto,sym('x(3)'));
Beta2DuePunti = subs(Beta2DuePunti,Beta2Punto,sym('x(4)'));
Beta2DuePunti = subs(Beta2DuePunti,TauD,sym('u(1)'));

% ThetaB = x(1)
% ThetaD = x(2)
% ThetaPuntoB = x(3)
% ThetaPuntoD = x(4)
% TauD = u(1)

%syms ThetaB ThetaPuntoB ThetaDuePuntiB ThetaD ThetaPuntoD ThetaDuePuntiD ;
%syms Jb Lbr Mbw L2 Md Jd TauD g;

%eqd1 = sym('(Jb + (Lbr^2)*Mbw + (L2^2)*Md)*ThetaDuePuntiB - g*(Lbr*Mbw + L2*Md)*sin(ThetaB) = -TauD');
%eqd2 = sym('Jd*(ThetaDuePuntiD + ThetaDuePuntiB) = TauD');

%eqd1 = subs(eqd1, 'ThetaDuePuntiD', solve(eqd2,'ThetaDuePuntiD'));
%eqd2 = subs(eqd2, 'ThetaDuePuntiB', solve(eqd1,'ThetaDuePuntiB'));

%ThetaDuePuntiB = solve(eqd1, 'ThetaDuePuntiB');
%ThetaDuePuntiD = solve(eqd2, 'ThetaDuePuntiD');

%ThetaDuePuntiB = subs(ThetaDuePuntiB,ThetaB,sym('x(1)'));
%ThetaDuePuntiB = subs(ThetaDuePuntiB,ThetaD,sym('x(2)'));
%ThetaDuePuntiB = subs(ThetaDuePuntiB,ThetaPuntoB,sym('x(3)'));
%ThetaDuePuntiB = subs(ThetaDuePuntiB,ThetaPuntoD,sym('x(4)'));
%ThetaDuePuntiB = subs(ThetaDuePuntiB,TauD,sym('u(1)'));

%ThetaDuePuntiD = subs(ThetaDuePuntiD,ThetaB,sym('x(1)'));
%ThetaDuePuntiD = subs(ThetaDuePuntiD,ThetaD,sym('x(2)'));
%ThetaDuePuntiD = subs(ThetaDuePuntiD,ThetaPuntoB,sym('x(3)'));
%ThetaDuePuntiD = subs(ThetaDuePuntiD,ThetaPuntoD,sym('x(4)'));
%ThetaDuePuntiD = subs(ThetaDuePuntiD,TauD,sym('u(1)'));



