% Script Matlab per la simulazione di un robot monociclo.
%
% Falanga Davide - M58/48
% Fontanelli Andrea - M58/64
%
% Il sistema � non lineare, quindi � necessaria una linearizzazione intorno
% ad un punto di equilibrio.

% Pulisco il workspace, chiudo altre finestre ed elimino il mondo \o/
clc
clear all
close all

%Tempo di campionamento
Tc=2*10^-3;


% Imposto i parametri del sistema 
Mbt = 1.475; % Massa del corpo
M1 = 1.475; % Massa del corpo
M2 = 0.342; % Massa del disco
%M2 = 0; % Massa del disco
Mw = 0.281; % Massa della ruota
Rw = 0.05; % Raggio della ruota
Jw = 0.000262; % Momento di inerzia della ruota intorno al baricentro
Jbt = 0.017097; % Momento di inerzia del corpo intorno al baricentro
J1a = 0.051625; % Momento di inerzia del corpo intorno al punto A
J2c = 0.003303; % Momento di inerzia del disco intorno al suo centro
L1 = 0.2916; % Distanza tra il punto A ed il centro del disco
L1c = 0.153; % Distanza tra il punto A ed il baricentro del corpo
Lbt = 0.103; % Distanza tra il centro della ruota ed il baricentro del corpo
g = 9.81; % Accelerazione di gravit�


Vp = 12; %Power voltage [V]

%Gear data
kr = 19; %Gear Reduction-ratio
%Motor data
I_max = 1.33; %Maximum continuos current [A]
La = 0.556 *1e-3 ; %Terminal inductance [H]
Ra = 2.4; %Terminal resistance [Ohm]
Ta = La/Ra; %Electrical time constant [s]
Kv =  1/(992.0*2*pi/60); %Speed constant [Vs/rad]
%Ke=0.0099; 
Kt = 6.3 *1e-3; %Torque constant [Nm/A]
Tm = 14.6 * 1e-3; %Mechanical time constant [s]
Jm = 45.3 * 1e-3*1e-4;  %Rotor inertia [kg m^2]



C_max = I_max * Kt * kr; %Maximum continuos torque [Nm]


% Effettuo la linearizzazione
x0=[0;0;0;0];
u0=[0];
[Ar,Br,Cr,Dr]=linmod('ruota_dee',x0,u0);
ruota=ss(Ar,Br,Cr,Dr)

% Osservabilit�
obsv_mat=obsv(ruota);
no_obsv=length(Ar)-rank(obsv_mat);

if no_obsv == 0
    disp 'Il sistema � osservabile.';
else
    disp 'Il sistema non � osservabile.';
end

% Controllabilit�
contr_mat=ctrb(ruota);
no_contr=length(Ar)-rank(contr_mat);

if no_contr == 0
    disp 'Il sistema � controllabile.';
else
    disp 'Il sistema non � controllabile.';
end

% Controllo LQR 
Qr=diag([2,20000,2,1]);
Rr=diag([2000]);
Kr=lqr(ruota,Qr,Rr);

%Assegnamento dei poli
Pr=[-0.5,-100,-2,-0.01];

Kr_p=place(Ar,Br,Pr);

%Controllo assegnamento dei poli
eig(Ar-Br*Kr_p)
